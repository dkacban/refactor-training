﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;

namespace Lab_6
{
    class WebDictionary
    {
        private static List<DictionaryWord> savedWords = new List<DictionaryWord>();
        private static List<DictionaryWord> lastSearchWords = new List<DictionaryWord>();

        static void Main(string[] args)
        {
            WebDictionary webDictionary = new WebDictionary();
            webDictionary.Start();
        }

        public void Start()
        {
            ProcessMenu();
        }

        private static void ProcessMenu()
        {
            bool running = true;

            PrintInfo();

            while (running)
            {
                Console.Write("dictionary>");
                String command = Console.ReadLine();

                if (command.Equals("Hello"))
                {
                    PrintInfo();
                }
                else if (command.StartsWith("search"))
                {
                    SearchWord(command);
                }
                else if (command.StartsWith("save"))
                {
                    SaveWord(command);
                }
                else if (command.StartsWith("showFound"))
                {
                    ShowFound();
                }
                else if (command.StartsWith("showSaved"))
                {
                    ShowSaved();
                }
                else if (command.StartsWith("exit"))
                {
                    running = false;
                }
            }
        }

        private static void ShowSaved()
        {
            foreach (var dictionaryElement in savedWords)
            {
                Console.WriteLine(dictionaryElement);
            }
        }

        private static void ShowFound()
        {
            int counter = 1;
            foreach (var dictionaryElement in lastSearchWords)
            {
                Console.WriteLine(counter++ + ") " + dictionaryElement);
            }
        }

        private static void SaveWord(String command)
        {
            int wordNumber = int.Parse(command.Split(' ')[1]);
            savedWords.Add(lastSearchWords[wordNumber - 1]);
        }

        private static void SearchWord(String command)
        {
            lastSearchWords.Clear();

            String polishWord = null;
            String englishWord = null;

            WebRequest request = HttpWebRequest.Create("http://www.dict.pl/dict?word=" + command.Split(' ')[1] + "&lang=PL");

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Stream responseStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding(response.CharacterSet));

                    String line = reader.ReadLine();

                    bool polish = true;
                    int counter = 1;

                    while (HasNextLine(line))
                    {
                        String pattern = ".*<a href=\"dict\\?word=(.*)&lang=.*";
                        Regex regex = new Regex(pattern, RegexOptions.IgnoreCase);
                        Match match = regex.Match(line);

                        while (match.Success)
                        {
                            if (polish)
                            {
                                Console.Write(counter + ") " + match.Groups[1] + " => ");
                                polishWord = match.Groups[1].ToString();
                                polish = false;
                            }
                            else
                            {
                                Console.WriteLine(match.Groups[1]);
                                englishWord = match.Groups[1].ToString();
                                lastSearchWords.Add(new DictionaryWord(polishWord, englishWord, DateTime.Now));
                                polish = true;
                                counter++;
                            }
                            match = match.NextMatch();
                        }

                        line = reader.ReadLine();
                    }

                    reader.Close();
                }
            }

        }

        private static bool HasNextLine(String line)
        {
            return line != null;
        }

        private static void PrintInfo()
        {
            Console.WriteLine("Welcome to Web Dictionary System.");
        }
    }
}
