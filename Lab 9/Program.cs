﻿using System;
using System.Collections.Generic;

namespace Lab_9
{
    class WebDictionary
    {
        private static List<DictionaryWord> savedWords = new List<DictionaryWord>();
        private static List<DictionaryWord> lastSearchWords = new List<DictionaryWord>();

        static void Main(string[] args)
        {
            WebDictionary webDictionary = new WebDictionary();
            webDictionary.Start();
            ProcessMenu();
        }

        public void Start()
        {
            ProcessMenu();
        }

        private static void ProcessMenu()
        {
            bool running = true;

            PrintInfo();

            while (running)
            {
                Console.Write("dictionary>");
                String command = Console.ReadLine();

                if (command.Equals("Hello"))
                {
                    PrintInfo();
                }
                else if (command.StartsWith("search"))
                {
                    SearchWord(command);
                }
                else if (command.StartsWith("save"))
                {
                    SaveWord(command);
                }
                else if (command.StartsWith("showFound"))
                {
                    ShowFound();
                }
                else if (command.StartsWith("showSaved"))
                {
                    ShowSaved();
                }
                else if (command.StartsWith("exit"))
                {
                    running = false;
                }
            }
        }

        private static void ShowSaved()
        {
            foreach (var dictionaryElement in savedWords)
            {
                Console.WriteLine(dictionaryElement);
            }
        }

        private static void ShowFound()
        {
            int counter = 1;
            foreach (var dictionaryElement in lastSearchWords)
            {
                Console.WriteLine(counter++ + ") " + dictionaryElement);
            }
        }

        private static void SaveWord(String command)
        {
            int wordNumber = int.Parse(command.Split(' ')[1]);
            savedWords.Add(lastSearchWords[wordNumber - 1]);
        }

        private static void SearchWord(String command)
        {
            SearchWordService searchWordService = new SearchWordService(command);
            lastSearchWords = searchWordService.Search();
        }

        private static void PrintInfo()
        {
            Console.WriteLine("Welcome to Web Dictionary System.");
        }
    }
}
