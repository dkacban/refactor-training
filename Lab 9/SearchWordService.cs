﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;

namespace Lab_9
{
    class SearchWordService
    {
        private readonly string command;
        private WebRequest request;
        StreamReader reader;

        public SearchWordService(String command)
        {
            this.command = command;
        }

        private WebRequest PrepareWebRequest()
        {
            String[] commandParts = command.Split(' ');
            String wordToFind = commandParts[1];
            String dictionaryUrl = "http://www.dict.pl/dict?word=" + wordToFind + "&lang=PL";
            WebRequest request = HttpWebRequest.Create(dictionaryUrl);

            return request;
        }

        public List<DictionaryWord> Search()
        {
            List<DictionaryWord> result = new List<DictionaryWord>();

            request = PrepareWebRequest();

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Stream responseStream = response.GetResponseStream();
                    reader = new StreamReader(responseStream, Encoding.GetEncoding(response.CharacterSet));
                    String currentWord = MoveToNextWord();

                    int counter = 1;

                    while (hasNextWord(currentWord))
                    {
                        DictionaryWord dictionaryWord = new DictionaryWord();
                        dictionaryWord.PolishWord = currentWord;
                        currentWord = MoveToNextWord();
                        dictionaryWord.EnglishWord = currentWord;
                        currentWord = MoveToNextWord();
                        result.Add(dictionaryWord);
                        Console.WriteLine(counter + ") " + dictionaryWord.PolishWord + " => " + dictionaryWord.EnglishWord);
                        counter++;
                    }

                    reader.Close();
                }
            }

            return result;
        }

        private String MoveToNextWord()
        {
            String line = reader.ReadLine();
            while(hasNextWord(line))
            {
                String pattern = ".*<a href=\"dict\\?word=(.*)&lang=.*";
                Regex regex = new Regex(pattern, RegexOptions.IgnoreCase);
                Match match = regex.Match(line);
                if (match.Success)
                {
                    var foundWord = match.Groups[1].ToString();
                    return foundWord;
                }
                else
                {
                    line = reader.ReadLine();
                }
            }

            return null;
        }

        private bool hasNextWord(String line)
        {
            return line != null;
        }
    }
}
