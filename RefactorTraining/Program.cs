﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RefactorTraining
{
    class Program
    {
        static void Main(string[] args)
        {
            bool ok = true;

            Console.WriteLine("Welcome to Web Dictionary System.");

            while (ok)
            {
                Console.Write("dictionary>");
                String c = Console.ReadLine();

                if(c.Equals("Hello"))
                {
                    Console.WriteLine("Welcome to Web Dictionary System.");
                }
                else if(c.StartsWith("search"))
                {
                    String f = c.Split(' ')[1];
                    //TODO searching
                }
                else if (c.StartsWith("save"))
                {
                    //TODO saving
                }
                else if (c.StartsWith("showFound"))
                {
                    //TODO showning found
                }
                else if (c.StartsWith("showSaved"))
                {
                    //TODO showing saved words
                }
                else if (c.StartsWith("exit"))
                {
                    ok = false;
                }
            }
        }
    }
}
