﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;

namespace Lab_8
{
    class SearchWordService
    {
        private string command = null;

        public SearchWordService(String command)
        {
            this.command = command;
        }

        public List<DictionaryWord> Search()
        {
            List<DictionaryWord> result = new List<DictionaryWord>();

            String polishWord = null;
            String englishWord = null;

            String[] commandParts = command.Split(' ');
            String wordToFind = commandParts[1];
            String dictionaryUrl = "http://www.dict.pl/dict?word=" + wordToFind + "&lang=PL";
            WebRequest request = HttpWebRequest.Create(dictionaryUrl);
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Stream responseStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding(response.CharacterSet));

                    String line = reader.ReadLine();

                    bool polish = true;
                    int counter = 1;

                    while (hasNextLine(line))
                    {
                        String pattern = ".*<a href=\"dict\\?word=(.*)&lang=.*";
                        Regex regex = new Regex(pattern, RegexOptions.IgnoreCase);
                        Match match = regex.Match(line);

                        while (match.Success)
                        {
                            if (polish)
                            {
                                Console.Write(counter + ") " + match.Groups[1] + " => ");
                                polishWord = match.Groups[1].ToString();
                                polish = false;
                            }
                            else
                            {
                                Console.WriteLine(match.Groups[1]);
                                englishWord = match.Groups[1].ToString();
                                result.Add(new DictionaryWord(polishWord, englishWord, DateTime.Now));
                                polish = true;
                                counter++;
                            }
                            match = match.NextMatch();
                        }

                        line = reader.ReadLine();
                    }

                    reader.Close();
                }
            }

            return result;
        }

        private bool hasNextLine(String line)
        {
            return line != null;
        }
    }
}
