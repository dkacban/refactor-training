﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;

namespace Lab_4
{
    class WebDictionary
    {
        private static List<DictionaryWord> list1 = new List<DictionaryWord>();
        private static List<DictionaryWord> list2 = new List<DictionaryWord>();

        static void Main(string[] args)
        {
            bool ok = true;

            Console.WriteLine("Welcome to Web Dictionary System.");

            while (ok)
            {
                Console.Write("dictionary>");
                String c = Console.ReadLine();

                if (c.Equals("Hello"))
                {
                    Console.WriteLine("Welcome to Web Dictionary System.");
                }
                else if (c.StartsWith("search"))
                {
                    list2.Clear();

                    String plW = null;
                    String enW = null;

                    WebRequest request = HttpWebRequest.Create("http://www.dict.pl/dict?word=" + c.Split(' ')[1] + "&lang=PL");

                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                    {
                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            Stream responseStream = response.GetResponseStream();
                            StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding(response.CharacterSet));

                            String line = reader.ReadLine();

                            bool polish = true;
                            int counter = 1;

                            while (line != null)
                            {
                                String pattern = ".*<a href=\"dict\\?word=(.*)&lang=.*";
                                Regex regex = new Regex(pattern, RegexOptions.IgnoreCase);
                                Match match = regex.Match(line);

                                while (match.Success)
                                {
                                    if (polish)
                                    {
                                        Console.Write(counter + ") " + match.Groups[1] + " => ");
                                        plW = match.Groups[1].ToString();
                                        polish = false;
                                    }
                                    else
                                    {
                                        Console.WriteLine(match.Groups[1]);
                                        enW = match.Groups[1].ToString();
                                        list2.Add(new DictionaryWord(plW, enW, DateTime.Now));
                                        polish = true;
                                        counter++;
                                    }
                                    match = match.NextMatch();
                                }

                                line = reader.ReadLine();
                            }

                            reader.Close();
                        }
                    }
                }
                else if (c.StartsWith("save"))
                {
                    int num = int.Parse(c.Split(' ')[1]);
                    list1.Add(list2[num - 1]);
                }
                else if (c.StartsWith("showFound"))
                {
                    int i = 1;
                    foreach(var el in list2)
                    {
                        Console.WriteLine(i++ + ") " + el);
                    }
                }
                else if (c.StartsWith("showSaved"))
                {
                    foreach(var el in list1)
                    {
                        Console.WriteLine(el);
                    }
                }
                else if (c.StartsWith("exit"))
                {
                    ok = false;
                }
            }
        }
    }
}
