﻿using System;

namespace Lab_7
{
    class DictionaryWord
    {
        public String PolishWord { get; set; }
        public String EnglishWord { get; set; }
        public DateTime DateTime { get; set; } 

        public DictionaryWord(String polishWord, String englishWord, DateTime dateTime)
        {
            PolishWord = polishWord;
            EnglishWord = englishWord;
            DateTime = dateTime;
        }

        public override string ToString()
        {
            return PolishWord + " => " + EnglishWord;
        }
    }
}
