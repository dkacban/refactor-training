﻿using System;
using System.Text;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;

namespace Lab_3
{
    class WebDictionary
    {
        static void Main(string[] args)
        {
            //http://www.dict.pl/dict?word=boy&&lang=PL
            WebRequest request = HttpWebRequest.Create("http://www.dict.pl/dict?word=" + "boy" + "&lang=PL");
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Stream responseStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding(response.CharacterSet));
                    String data = reader.ReadToEnd();

                    String pattern = ".*<a href=\"dict\\?word=(.*)&lang=.*";
                    Regex regex = new Regex(pattern, RegexOptions.IgnoreCase);
                    Match match = regex.Match(data);
                    int i = 1;
                    bool polish = true;
                    while (match.Success)
                    {
                        if (polish)
                        {
                            Console.Write(i + ") " + match.Groups[1] + " => ");
                            polish = false;
                        }
                        else
                        {
                            Console.WriteLine(match.Groups[1]);
                            polish = true;
                            i++;
                        }

                        match = match.NextMatch();
                    }

                    reader.Close();
                }
            }   
        }
    }
}
