﻿using System;
using System.Text;
using System.Net;
using System.IO;

namespace Lab_2
{
    class WebDictionary
    {
        static void Main(string[] args)
        {
            //http://www.dict.pl/dict?word=boy&words=&lang=PL

            WebRequest request = HttpWebRequest.Create("http://www.dict.pl/dict?word=" + "boy" + "&words=&lang=PL");
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Stream responseStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding(response.CharacterSet));

                    String data = reader.ReadToEnd();
                    reader.Close();
                    Console.WriteLine(data);
                }
            }
        }
    }
}
