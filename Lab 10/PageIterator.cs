﻿using System;
using System.Text;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
namespace Lab_10
{
    class PageIterator
    {
        private WebRequest request;
        StreamReader reader;

        public PageIterator(String wordToFind)
        {
            request = PrepareWebRequest(wordToFind);

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Stream responseStream = response.GetResponseStream();
                reader = new StreamReader(responseStream, Encoding.GetEncoding(response.CharacterSet));
            }
        }

        private WebRequest PrepareWebRequest(String wordToFind)
        {
            /* this is not responsibility of PageIterator. word will be delivered to constructor */
            //            String[] commandParts = command.Split(' ');
            //            String wordToFind = commandParts[1];
            String dictionaryUrl = "http://www.dict.pl/dict?word=" + wordToFind + "&lang=PL";
            WebRequest request = HttpWebRequest.Create(dictionaryUrl);

            return request;
        }

        public String MoveToNextWord()
        {
            String line = reader.ReadLine();

            while (HasNextWord(line))
            {
                String pattern = ".*<a href=\"dict\\?word=(.*)&lang=.*";
                Regex regex = new Regex(pattern, RegexOptions.IgnoreCase);
                Match match = regex.Match(line);
                if (match.Success)
                {
                    var foundWord = match.Groups[1].ToString();
                    return foundWord;
                }
                else
                {
                    line = reader.ReadLine();
                }
            }

            return null;
        }

        public bool HasNextWord(String line)
        {
            return line != null;
        }

        public void Dispose()
        {
            reader.Dispose();
        }
    }
}
