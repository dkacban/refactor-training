﻿using System;
using System.Collections.Generic;

namespace Lab_10
{
    //New responsibility: Only algorithm
    class SearchWordService
    {
        //not necessary field - it made this class storing state. Deleted.
//        private string command = null;

        //deleted constructor parameter: command
        public SearchWordService()
        {
//            this.command = command;
        }

        //Adding parameter
        public List<DictionaryWord> Search(String wordToFind)
        {
            List<DictionaryWord> result = new List<DictionaryWord>();
            PageIterator pageIterator = new PageIterator(wordToFind);
            String currentWord = pageIterator.MoveToNextWord();
            int counter = 1;

            while (pageIterator.HasNextWord(currentWord))
            {
                DictionaryWord dictionaryWord = new DictionaryWord();
                dictionaryWord.PolishWord = currentWord;
                currentWord = pageIterator.MoveToNextWord();
                dictionaryWord.EnglishWord = currentWord;
                currentWord = pageIterator.MoveToNextWord();

                result.Add(dictionaryWord);
                Console.WriteLine(counter + ") " + dictionaryWord.PolishWord + " => " + dictionaryWord.EnglishWord);
                counter++;
            }

            // closing of stram reader moved to PageIterator
            //reader.Close();
            pageIterator.Dispose();

            return result;
        }
    }
}
